﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using Photon.Pun;
using UnityEngine;
using System.Collections.Generic;

namespace Pugnacies
{
    using PhotonPlayer = Photon.Realtime.Player;

    [RequireComponent(typeof(PhotonView))]
    public class HumanPlayer : InGamePlayer
    {
        private string playerPrefabName = "Avatar";

        public override string Name { get { return photonPlayer.NickName; } }

        private PhotonPlayer photonPlayer;
        public PhotonPlayer PhotonPlayer
        {
            get { return photonPlayer; }
            set { photonPlayer = value; }
        }

        private static HumanPlayer localPlayer;
        public static HumanPlayer LocalPlayer { get { return localPlayer; } }

        private Ability[] readyToUseAbilities = new Ability[8];

        protected override void Awake()
        {
            base.Awake();

            if (!(photonView.IsMine || PhotonNetwork.OfflineMode))
                return;

            avatar = PhotonNetwork.Instantiate(playerPrefabName, InGameLayer.PlayerSpawnLocation, Quaternion.identity).GetComponent<Avatar>();
            localPlayer = this;
        }

        void Start()
        {
            CameraManager.CurrentCamera.transform.position = new Vector3(
                avatar.transform.position.x,
                CameraManager.CurrentCamera.transform.position.y,
                avatar.transform.position.z);
        }

        [UpdateOnly]
        private void HandleMovementInput()
        {
            if (Input.GetKey(KeyCode.Mouse1))
            {
                RaycastHit hit;
                Camera camera = CameraManager.CurrentCamera.GetComponent<Camera>();
                Ray ray = camera.ScreenPointToRay(Input.mousePosition);

                Physics.Raycast(ray, out hit);

                avatar.WalkTowards(hit.point);
            }
        }

        [UpdateOnly]
        private void HandleAbilityCasts()
        {
            int whichAbilityIndex;
            if (Input.GetKeyDown(UserSettings.currentSettings.spell1))
            {
                whichAbilityIndex = 1;
            }
        }

        protected override void Update()
        {
            base.Update();

            if (!photonView.IsMine && !PhotonNetwork.OfflineMode)
                return;
            HandleMovementInput();
        }
    }
}