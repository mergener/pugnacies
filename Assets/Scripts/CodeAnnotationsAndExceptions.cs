﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using System;

namespace Pugnacies
{
    #region Exceptions
    /// <summary>
    /// To be thrown when a supposed singleton pattern is broken.
    /// </summary>
    public class SingletonDuplicateException : Exception
    {
        public SingletonDuplicateException()
            : base("An undesired singleton was created.")
        { }
    }
    #endregion Exceptions

    #region Annotations
    //
    // The following annotations are merely read-only.
    //

    #region Pure

    /// <summary>
    /// Signs that a method should only be called within the Update callback function.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class UpdateOnlyAttribute : Attribute { }

    /// <summary>
    /// Signs that the specified symbol is synchronized throughout the room network. Peek into CodeAnnotations.cs to
    /// read more.
    /// </summary>
    /// 
    /// InSync rules:
    /// 
    /// For methods:
    ///     - The method must effectively* work the same way throughout all clients in the room;
    ///     
    /// For fields:
    ///     - The field must have the same effective* value throughout all clients in the same room.
    ///     
    /// For properties:
    ///     - Every 'get' property must always return the same value in all clients.
    ///     
    /// For events:
    ///     - When fired in one client, the event must be fired in every other client in the same room.
    ///     
    /// For classes/structs:
    ///     - Implies that all public members of the class are implicitly decorated with InSync attribute,
    ///     thus which each of them following its respective guidelines to be in sync.
    ///     
    /// Note that you can implement InSync any way you desire, since it follows the rules above.
    public class InSyncAttribute : Attribute { }

    /// <summary>
    /// Signs that symbols that derive from the specified symbol must implement the rules of InSyncAttribute.
    /// Derived symbols may be sub-classes/structs, method/property overrides or event calls in overriden methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Method | AttributeTargets.Property)]
    public class MustBeInSyncAttribute : Attribute { }

    /// <summary>
    /// Signs that the specified class must not be instantiated during in-game runtime.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public class CreateBeforeGameAttribute : Attribute { }

    #endregion Pure Annotations

    //
    // The following annotations have an actual effect on code.
    //

    #region Effective

    /// <summary>
    /// Marks the following spell class as not purchasable.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class NonPurchasableAttribtue : Attribute { }

    #endregion Effective
    #endregion Annotations
}