﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using TMPro;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using System.Text;

namespace Pugnacies
{
    [System.Serializable]
    public class Lobby : MonoBehaviourPunCallbacks
    {
        [SerializeField]
        private GameObject commonView; // viewed by everyone

        [SerializeField]
        private GameObject hostView; // viewed only by host

        [SerializeField]
        private GameObject guestView; // viewed only by guests

        [SerializeField]
        private TextMeshProUGUI playerListText;

        #region Host Only
        public void StartMatch()
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
            PhotonNetwork.CurrentRoom.IsVisible = false;
            PhotonNetwork.LoadLevel(1);
        }
        #endregion Host Only

        #region Guest Only

        public override void OnMasterClientSwitched(Player newMasterClient)
        {
            base.OnMasterClientSwitched(newMasterClient);
            if (newMasterClient == PhotonNetwork.LocalPlayer)
            {
                guestView.SetActive(false);
                hostView.SetActive(true);
            }
        }

        public override void OnEnable()
        {
            base.OnEnable();
            commonView.SetActive(true);
            PhotonNetwork.AutomaticallySyncScene = true;

            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                hostView.SetActive(true);
            }
            else
                guestView.SetActive(true);

            UpdatePlayerNames();
        }


        #endregion Guest Only

        public void LeaveLobby()
        {
            PhotonNetwork.LeaveRoom();
        }

        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            base.OnPlayerEnteredRoom(newPlayer);
            UpdatePlayerNames();
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            base.OnPlayerLeftRoom(otherPlayer);
            UpdatePlayerNames();
        }

        private void UpdatePlayerNames()
        {
            var playersDict = PhotonNetwork.CurrentRoom.Players;
            StringBuilder sb = new StringBuilder(PhotonNetwork.CurrentRoom.PlayerCount);
            
            foreach (int i in playersDict.Keys)
            {
                if (playersDict[i].IsMasterClient)
                {
                    sb.Append(playersDict[i].NickName + " (Host) \n");
                }
                else
                {
                    sb.Append(playersDict[i].NickName + "\n");
                }
            }

            playerListText.text = sb.ToString();
        }
    }
}