﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using Photon.Pun;

namespace Pugnacies
{
    [System.Serializable]
    public class RootMenu : MonoBehaviourPunCallbacks
    {
        #region References
        [SerializeField]
        private Slider musicVolumeSlider;

        [SerializeField]
        private GameObject mainMenu;
        public static GameObject MainMenu { get { return singleton.mainMenu; } }

        [SerializeField]
        private Slider soundEffectsVolumeSlider;

        [SerializeField]
        private GameObject multiplayerHubGO;
        #endregion

        #region Main Menu
        public void SingleplayerButton()
        {
            PhotonNetwork.OfflineMode = true;
            WarningWindow.SendWarning("Warning", "You are entering offline mode.");
            mainMenu.gameObject.SetActive(false);
            multiplayerHubGO.SetActive(true);
        }

        public void MultiplayerButton()
        {
            WarningWindow.SendWarning("Warning", "Multiplayer accounts are not yet implemented. Choose a username and login without a password.");
        }

        public void CreditsButton()
        {
            WarningWindow.SendWarning("Error", "The credits menu is still not available.");
        }

        public void ExitButton()
        {
#if UNITY_EDITOR
            EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        #endregion Main Menu

        #region Options Menu
        public void RequestUserSettingsReset()
        {
            UserSettings.ResetUserSettings();
        }
        #region Audio Settings Menu
        public void ApplyAudioChanges()
        {
            UserSettings.currentSettings.MusicVolume = musicVolumeSlider.value;
            UserSettings.currentSettings.SoundEffectsVolume = soundEffectsVolumeSlider.value;
        }
        #endregion Audio Settings Menu

        #endregion Options Menu
        
        public void LoadUserSettings()
        {
            // Audio Settings
            soundEffectsVolumeSlider.value = UserSettings.currentSettings.SoundEffectsVolume;
            musicVolumeSlider.value = UserSettings.currentSettings.MusicVolume;
        }

        void Start()
        {
            LoadUserSettings();
            UserSettings.OnSettingsReset += LoadUserSettings;
        }

        private static RootMenu singleton = null;
        public static RootMenu Singleton { get { return singleton; } }

        void Awake()
        {
            if (singleton == null)
            {
                singleton = this;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        void OnApplicationQuit()
        {
            UserSettings.SaveSettings();
        }
    }
}