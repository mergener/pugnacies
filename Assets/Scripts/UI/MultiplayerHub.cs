﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Pugnacies
{
    public class MultiplayerHub : MonoBehaviourPunCallbacks
    {
        #region Initial Screen
        [SerializeField]
        private GameObject initialScreen;

        [SerializeField]
        private GameObject findMatchWindow;

        [SerializeField]
        private InputField matchNameField;

        [SerializeField]
        private Lobby lobbyView;

        [SerializeField]
        private GameObject uiBlocker;

        [SerializeField]
        private GameObject matchCreator;

        public static MultiplayerHub Singleton { get; private set; }

        public void DisconnectAndReturn()
        {
            PhotonNetwork.Disconnect();
            RootMenu.MainMenu.SetActive(true);
        }

        private Coroutine findMatchCoroutine = null;

        public void FindMatch()
        {
            findMatchWindow.SetActive(true);
            findMatchCoroutine = StartCoroutine(FindMatchCoroutine());
        }

        private IEnumerator FindMatchCoroutine()
        {
            PhotonNetwork.JoinRandomRoom();
            yield return new WaitForEndOfFrame();
        }

        public void StopFindingMatch()
        {
            if (findMatchCoroutine != null)
            {
                StopCoroutine(findMatchCoroutine);
                findMatchCoroutine = null;
            }

            findMatchWindow.SetActive(false);
        }

        public override void OnJoinedRoom()
        {
            base.OnJoinedRoom();
            StopFindingMatch();

            uiBlocker.SetActive(false);

            matchCreator.SetActive(false);
            lobbyView.gameObject.SetActive(true);
        }

        public void CreateMatch()
        {
            var options = new RoomOptions
            {
                IsVisible = true,
                MaxPlayers = 12,
                EmptyRoomTtl = 20000, // 20 secs
                IsOpen = true
            };

            PhotonNetwork.CreateRoom(matchNameField.text, options);
            uiBlocker.SetActive(true);
        }

        public override void OnLeftRoom()
        {
            base.OnLeftRoom();
            lobbyView.gameObject.SetActive(false);
            initialScreen.SetActive(true);
        }
        #endregion Initial Screen

        public override void OnDisconnected(DisconnectCause cause)
        {
            base.OnDisconnected(cause);
            this.gameObject.SetActive(false);
        }
    }
}