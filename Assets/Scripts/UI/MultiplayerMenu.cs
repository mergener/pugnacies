﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using Photon.Pun;
using Photon.Realtime;

namespace Pugnacies
{
    public class MultiplayerMenu : MonoBehaviourPunCallbacks
    {
        private static MultiplayerMenu singleton;
        public static MultiplayerMenu Singleton { get { return singleton; } }

        [SerializeField]
        private GameObject uiBlocker;

        [SerializeField]
        private GameObject multiplayerHubGO;

        [SerializeField]
        private GameObject initialPage;

        [SerializeField]
        private InputField loginUsernameField;

        [SerializeField]
        private InputField passwordUsernameField;

        private bool tryingToConnectToMaster = false;

        #region Login     
        private static readonly Regex usernameRegex = new Regex("^[a-zA-Z0-9]*$");

        public void Login()
        {
            if (!usernameRegex.IsMatch(loginUsernameField.text) || loginUsernameField.text.Length < 5)
            {
                WarningWindow.SendWarning(
                    "Login Error", 
                    "Your username does not match the desired pattern. Use only alphanumeric characters without spaces and at least five characters."
                );
                return;
            }

            PhotonNetwork.NickName = loginUsernameField.text;
            PhotonNetwork.ConnectUsingSettings();
            uiBlocker.SetActive(true);
            tryingToConnectToMaster = true;
        }
        #endregion Login

        #region Photon Callbacks

        public override void OnDisconnected(DisconnectCause cause)
        {
            uiBlocker.SetActive(false);
            if (cause == DisconnectCause.DisconnectByClientLogic)
            {
                initialPage.SetActive(true);
                RootMenu.MainMenu.SetActive(true);
                this.gameObject.SetActive(false);
            }
            else if (tryingToConnectToMaster)
            {
                uiBlocker.SetActive(false);
                WarningWindow.SendWarning("Connection error", "A connection error occurred while trying to connect to Photon Servers: " + cause.ToString());
            }
            else
            {
                switch (cause)
                {
                    default:
                        WarningWindow.SendWarning("Disconnected", "You have been disconnected from the Photon Servers.");
                        break;
                }
                RootMenu.MainMenu.SetActive(true);
            }
        }

        public override void OnConnectedToMaster()
        {
            this.gameObject.SetActive(false);
            uiBlocker.SetActive(false);
            tryingToConnectToMaster = false;
            multiplayerHubGO.SetActive(true);

            WarningWindow.SendWarning("Welcome", "Succesfully joined Photon Servers. Welcome, " + PhotonNetwork.LocalPlayer.NickName + "!");
        }

        #endregion Photon Callbacks

        void Awake()
        {
            if (singleton == null)
            {
                singleton = this;
            }
            else if (singleton != this)
            {
                Destroy(this.gameObject);
            }
        }

    }
}