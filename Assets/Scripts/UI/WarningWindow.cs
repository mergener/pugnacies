﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using System.Collections.Generic;
using TMPro;

namespace Pugnacies
{
    /// <summary>
    /// Part of the CommonLayer, the WarningWindow contains a title, a description text and an OK button.
    /// </summary>
    public class WarningWindow : MonoBehaviour
    {
        [SerializeField]
        private GameObject window;
        
        [SerializeField]
        private TextMeshProUGUI titleText;

        [SerializeField]
        private TextMeshProUGUI contentText;

        private struct Warning
        {
            public string title;
            public string content;
        }

        private static WarningWindow singleton = null;

        private static Queue<Warning> warningQueue = new Queue<Warning>();

        private static bool isShowingWindow = false;

        public static void SendWarning(string title, string content)
        {
            if (isShowingWindow)
            {
                warningQueue.Enqueue(
                    new Warning
                    {
                        title = title,
                        content = content
                    }
                );
            }
            else
            {
                isShowingWindow = true;
                singleton.titleText.text = title;
                singleton.contentText.text = content;
                singleton.window.SetActive(true);
            }
        }

        public static void PassWarning()
        {
            if (warningQueue.Count > 0)
            {
                var w = warningQueue.Dequeue();
                singleton.titleText.text = w.title;
                singleton.contentText.text = w.content;
            }
            else
            {
                isShowingWindow = false;
                singleton.window.SetActive(false);
            }
        }

        public void GOPassWarning()
        {
            PassWarning();
        }

        void Awake()
        {
            if (singleton == null)
            {
                singleton = this;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }
}
