﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine.UI;
using UnityEngine;

namespace Pugnacies
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField]
        private Image healthBarFill;

        void LateUpdate()
        {
            healthBarFill.fillAmount = Mathf.Lerp(
                healthBarFill.fillAmount, 
                HumanPlayer.LocalPlayer.Avatar.Health / HumanPlayer.LocalPlayer.Avatar.MaxHealth,
                0.05f);
        }
    }
}