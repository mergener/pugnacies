﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.  
 *                                                         
*/

using System.Collections.Generic;

namespace Pugnacies
{
    /// <summary>
    /// Defines the behaviour of an order.
    /// Note: use negative numbers for indefinite durations orders.
    /// </summary>
    [CreateBeforeGame]
    public struct AvatarOrder
    {
        private static List<AvatarOrder> allOrders = new List<AvatarOrder>();
        public static readonly AvatarOrder Idle = new AvatarOrder(true, -1f);
        public static readonly AvatarOrder Moving = new AvatarOrder(true, -1f);

        private int id;
        public int ID { get { return id; } }

        private bool interruptable;
        public bool Interruptable { get { return interruptable; } }

        private float duration;
        public float Duration { get { return duration; } }

        public static bool operator ==(AvatarOrder a, AvatarOrder b)
        {
            return a.id == b.id;
        }

        public static bool operator !=(AvatarOrder a, AvatarOrder b)
        {
            return a.id != b.id;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public static AvatarOrder GetWithID(int id)
        {
            return allOrders[id];
        }
        
        public AvatarOrder(bool interruptable = true, float duration = 0f)
        {
            this.interruptable = interruptable;
            this.duration = duration;

            id = allOrders.Count;
            allOrders.Add(this);
        }
    }
}