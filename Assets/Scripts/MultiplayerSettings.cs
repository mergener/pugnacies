﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;

namespace Pugnacies
{
    public class MultiplayerSettings : MonoBehaviour
    {
        private static MultiplayerSettings singleton;

        [SerializeField]
        private bool delayStart;
        public static bool DelayStart { get { return singleton.delayStart; } }

        [SerializeField]
        private int maxPlayers;
        public static int MaxPlayers { get { return singleton.maxPlayers; } }

        [SerializeField]
        private int menuScene;
        public static int MenuScene { get { return singleton.menuScene; } }

        [SerializeField]
        private int multiplayerScene;
        public static int MultiplayerScene { get { return singleton.multiplayerScene; } }

        void Awake()
        {
            // make singleton
            if (singleton == null)
            {
                singleton = this;
            }
            else if (singleton != this)
            {
                Debug.LogWarning("MultiplayerSettings: Singleton duplicate warning!");
                Destroy(this);
            }
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
