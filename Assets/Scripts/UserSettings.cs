﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                       
*/

using UnityEngine;
using System.IO;

namespace Pugnacies
{
    public class UserSettings
    {
        public delegate void OnSettingsResetDel();
        public static event OnSettingsResetDel OnSettingsReset;

        private const string settingsDirectory = "dat/";
        private const string settingsFileName = "conf.json";

        public static UserSettings defaultSettings = new UserSettings();
        public static UserSettings currentSettings;

        #region Input Settings
        public KeyCode
            stop = KeyCode.S,

            spell1 = KeyCode.Q,
            spell2 = KeyCode.W,
            spell3 = KeyCode.E,
            spell4 = KeyCode.R,
            spell5 = KeyCode.F,
            spell6 = KeyCode.T,
            spell7 = KeyCode.F,
            spell8 = KeyCode.G,

            item1 = KeyCode.Alpha1,
            item2 = KeyCode.Alpha2,
            item3 = KeyCode.Alpha3,
            item4 = KeyCode.Alpha4,

            chat = KeyCode.Return;
        #endregion Input Settings

        #region Gameplay Settings

        #endregion Gameplay Settings

        #region Audio Settings
        [SerializeField]
        private float soundEffectsVolume = 1f;
        public float SoundEffectsVolume
        {
            get { return soundEffectsVolume; }
            set
            {
                if (value > 1f)
                {
                    soundEffectsVolume = 1f;
                }
                else if(value < 0f)
                {
                    soundEffectsVolume = 0f;
                }
                else
                {
                    soundEffectsVolume = value;
                }
            }
        }

        [SerializeField]
        private float musicVolume = 1f;
        public float MusicVolume
        {
            get { return musicVolume; }
            set
            {
                if (value > 1f)
                {
                    musicVolume = 1f;
                }
                else if (value < 0f)
                {
                    musicVolume = 0f;
                }
                else
                {
                    musicVolume = value;
                }
            }
        }
        #endregion Audio Settings

        public static void ResetUserSettings()
        {
            currentSettings = new UserSettings();

            if (OnSettingsReset != null)
                OnSettingsReset();
        }

        [RuntimeInitializeOnLoadMethod]
        public static void OnRunGame()
        {
            // we need to load existing data files for settings. If not, use the defaultSettings.
            Directory.CreateDirectory(settingsDirectory);

            if (File.Exists(settingsDirectory + settingsFileName))
            {
                var json = File.ReadAllText(settingsDirectory + settingsFileName);
                currentSettings = JsonUtility.FromJson<UserSettings>(json);
            }
            else
            {
                var json = JsonUtility.ToJson(defaultSettings, false);
                File.WriteAllText(settingsDirectory + settingsFileName, json);
                currentSettings = new UserSettings();
            }
        }

        public static void SaveSettings()
        {
            var json = JsonUtility.ToJson(currentSettings);
            File.WriteAllText(settingsDirectory + settingsFileName, json);
        }

        private UserSettings() { }
    }
}