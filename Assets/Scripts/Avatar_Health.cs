﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace Pugnacies
{
    public partial class Avatar : MonoBehaviourPun, IPunObservable
    {
        private bool alive = true;
        public bool Alive { get { return alive; } }

        // On offline mode, the health serializer is null.
        // On online mode, we will send the data from the health serializer onto the network, updating other players.
        // Note that this gets set to dirty whenever the Health setter is called, so health regeneration will not be
        // spamming the stream.
        private class HealthSerializer
        {
            private bool dirty = false;
            private float healthDelta = 0f;
            private float maxHealthDelta = 0f;
            private float healthRegenDelta = 0f;

            public bool Dirty { get { return dirty; } }
            public float HealthDelta
            {
                get { return healthDelta; }
                set
                {
                    healthDelta = value;
                    dirty = true;
                }
            }
            public float MaxHealthDelta
            {
                get { return maxHealthDelta; }
                set
                {
                    maxHealthDelta = value;
                    dirty = true;
                }
            }
            public float HealthRegenDelta
            {
                get { return healthRegenDelta; }
                set
                {
                    healthRegenDelta = value;
                    dirty = true;
                }
            }

            public void Reset()
            {
                healthDelta = 0f;
                maxHealthDelta = 0f;
                healthRegenDelta = 0f;
                dirty = false;
            }
        }
        private HealthSerializer healthSerializer = null;

        [SerializeField]
        private float health = 100;
        public float Health
        {
            get { return health; }
            set
            {
                var oldHealth = health;

                if (value > maxHealth)
                {
                    health = maxHealth;
                }
                else if (health <= 0)
                {
                    Die();
                }
                else
                {
                    health = value;
                }
                
                if (!PhotonNetwork.OfflineMode)
                {
                    healthSerializer.HealthDelta += health - oldHealth;
                }
            }
        }

        [SerializeField]
        private float maxHealth = 100;
        public float MaxHealth
        {
            get { return maxHealth; }
            set
            {
                var proportion = health / maxHealth;

                if (!PhotonNetwork.OfflineMode)
                {
                    healthSerializer.MaxHealthDelta += value - maxHealth;
                }

                maxHealth = value;
                Health = maxHealth * proportion;
            }
        }

        [SerializeField]
        private float healthRegen = 0.4f;
        public float HealthRegen
        {
            get { return healthRegen; }
            set
            {
                if (!PhotonNetwork.OfflineMode)
                {
                    healthSerializer.HealthRegenDelta += value - healthRegen;
                }

                healthRegen = Mathf.Abs(value);
            }
        }

        public delegate void OnDeathDel(Avatar a);
        public event OnDeathDel OnDeath;

        /// <summary>
        /// Kills this avatar, setting its health to zero, alive flag to false and notifying other players.
        /// </summary>
        public void Die()
        {
            health = 0;

            if (OnDeath != null)
            {
                OnDeath(this);
            }
        }

        public const float damageInfoLifespan = 30f;
        private List<DamageInfo> recentDamageInfo = new List<DamageInfo>();

        public delegate void OnDamageTakenDel(float dmg, Avatar source);
        public event OnDamageTakenDel OnDamageTaken;

        public void TakeDamage(float amount, Avatar source = null, DamageType damageType = DamageType.Physical)
        {
            float finalAmount;

            switch (damageType)
            {
                default:
                    finalAmount = amount;
                    break;
            }

            photonView.RPC("ApplyDamageSuffering", RpcTarget.All, finalAmount, source ? source.photonView.ViewID : -1, damageType, InGameLayer.ElapsedTime);
        }

        [PunRPC]
        private void ApplyDamageSuffering(float amount, int sourcePhotonViewID, DamageType damageType, float time)
        {
            if (sourcePhotonViewID != -1)
            {
                var source = PhotonView.Find(sourcePhotonViewID).GetComponent<Avatar>();
                StartCoroutine(AddRecentDamageCoroutine(new DamageInfo(source, amount, time, damageType)));

                if (OnDamageTaken != null)
                    OnDamageTaken(amount, source);
            }
            else
            {
                StartCoroutine(AddRecentDamageCoroutine(new DamageInfo(null, amount, time, damageType)));

                if (OnDamageTaken != null)
                    OnDamageTaken(amount, null);
            }
        }

        private IEnumerator AddRecentDamageCoroutine(DamageInfo damageInfo)
        {
            recentDamageInfo.Add(damageInfo);
            yield return new WaitForSeconds(damageInfoLifespan);
            recentDamageInfo.RemoveAt(0); // we can remove at index zero since the recentDamageInfo list will behave as a queue.
        }

        /// <summary>
        /// Returns information about all damage taken in the last 's' seconds. 
        /// Remember that damage lifetime is limited by the constant 'damageInfoLifespan'.
        /// </summary>
        public List<DamageInfo> GetLatestDamageInfo(float s)
        {
            var ret = new List<DamageInfo>();

            foreach (var info in recentDamageInfo)
            {
                if (info.Time < InGameLayer.ElapsedTime - s)
                    break;

                ret.Add(info);
            }

            return ret;
        }

        public struct DamageInfo
        {
            public Avatar Source { get; private set; }
            public float Amount { get; private set; }
            public DamageType DamageType { get; private set; }

            /// <summary>
            /// The match elapsed time at the moment this damage was dealt.
            /// </summary>
            public float Time { get; private set; }

            public DamageInfo(Avatar source, float amount, float time, DamageType damageType)
            {
                this.Source = source;
                this.Amount = amount;
                this.Time = time;
                this.DamageType = damageType;
            }
        }

        private void InitalizeHealthSerializer()
        {
            if (!PhotonNetwork.OfflineMode)
            {
                healthSerializer = new HealthSerializer();
            }
        }

        private const float healthRegenInterval = 0.2f;
        private IEnumerator RegenerateHealth()
        {
            // By putting the health regeneration on a coroutine, we can relieve a bit of the Update function.
            // Also, to avoid unnecessary traffic onto the network, we do not use the Health setter, so less data is sent.
            while (true)
            {
                if (alive)
                {
                    var healthAfterRegen = health + healthRegen;
                    if (healthAfterRegen > maxHealth)
                    {
                        health = maxHealth;
                    }
                    else if (health <= 0)
                    {
                        Die();
                    }
                    else
                    {
                        health = healthAfterRegen;
                    }
                }
                yield return new WaitForSeconds(healthRegenInterval);
            }
        }

        private void SerializeViewHealth(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(healthSerializer.Dirty);
                if (healthSerializer.Dirty)
                {
                    // since the serializer is dirty, lets send the deltas
                    stream.SendNext(healthSerializer.HealthDelta);
                    stream.SendNext(healthSerializer.MaxHealthDelta);
                    stream.SendNext(healthSerializer.HealthRegenDelta);

                    // now, zero out the serializer and remove the dirty flag
                    healthSerializer.Reset();
                }
            }
            else
            {
                if ((bool)stream.ReceiveNext())
                {
                    // health is dirty, lets update.
                    health += (float)stream.ReceiveNext();
                    maxHealth += (float)stream.ReceiveNext();
                    healthRegen += (float)stream.ReceiveNext();
                }
            }
        }
    }

    public enum DamageType
    {
        Physical,
        Fire,
        Ice,
        Nature,
        Arcane
    }
}