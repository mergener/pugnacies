﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using System.Reflection;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Pugnacies
{
    public abstract class Ability : ScriptableObject
    {
        #region Ability information
        [SerializeField]
        [TextArea(4, 4)]
        protected string tooltip = "No tooltip.";
        public virtual string GetTooltip(Avatar a) { return tooltip; }

        #region Icons
        [SerializeField]
        protected Sprite icon_NormalEnabled;
        public Sprite Icon_NormalEnabled { get { return icon_NormalEnabled; } }

        [SerializeField]
        protected Sprite icon_NormalDisabled;
        public Sprite Icon_NormalDisabled
        {
            get
            {
                if (icon_NormalDisabled != null)
                {
                    return icon_NormalDisabled;
                }
                else
                {
                    return Icon_NormalEnabled;
                }
            }
        }

        [SerializeField]
        protected Sprite icon_OnShopEnabled;
        public Sprite Icon_OnShopEnabled
        {
            get
            {
                if (icon_OnShopEnabled != null)
                {
                    return icon_OnShopEnabled;
                }
                else
                {
                    return Icon_NormalEnabled;
                }
            }
        }

        [SerializeField]
        protected Sprite icon_OnShopDisabled;
        public Sprite Icon_OnShopDisabled
        {
            get
            {
                if (icon_OnShopDisabled != null)
                {
                    return icon_OnShopDisabled;
                }
                else
                {
                    return Icon_NormalDisabled;
                }
            }
        }
        #endregion Icons
        #endregion Ability information

        #region Ability stats and functionality
        [SerializeField]
        protected int maxLevel;
        public int MaxLevel { get { return maxLevel; } }

        [SerializeField]
        protected int goldCost = 0;
        public int GoldCost { get { return goldCost; } }

        [SerializeField]
        protected int requiredLevel = 0;
        public int RequiredLevel { get { return RequiredLevel; } }

        [SerializeField]
        protected float cooldown = 0;
        public float Cooldown { get { return cooldown; } }

        [SerializeField]
        protected AbilityCastMode castMode = AbilityCastMode.Normal;
        public AbilityCastMode CastMode
        {
            get { return castMode; }
            set { castMode = value; }
        }
        

        public virtual bool IsUsable(Avatar a, object target = null) { return true; }
        public virtual void OnUse(Avatar a, object target = null) { }
        public virtual void OnStartUse(Avatar a, object target = null) { }
        public virtual void OnFinishUse(Avatar a, object target = null) { }
        public virtual void OnCancelUse(Avatar a, object target = null) { }
        public virtual void OnLearn(Avatar a) { }
        public virtual void OnUnlearn(Avatar a) { }
        public virtual void OnLevelChange(Avatar a, int oldLevel, int newLevel) { }
        #endregion Ability stats and functionality

        #region Internal
        private static List<Ability> allAbilities = new List<Ability>();
        private int id;
        public int ID { get { return id; } }

        public static Ability GetWithID(int id)
        {
            return allAbilities[id];
        }

        private static Dictionary<Type, int> abilityTypesToIds = new Dictionary<Type, int>();

        public static T Get<T>() where T : Ability
        {
            return (T)GetWithID(abilityTypesToIds[typeof(T)]);
        }
        
        //[RuntimeInitializeOnLoadMethod]
        private static void InitializeAbilities()
        {
            var assembly = Assembly.GetAssembly(typeof(Ability));

            var abilityTypes = from abil in assembly.GetTypes()
                               where abil.IsSubclassOf(typeof(Ability)) && abil != typeof(Ability)
                               select abil;

            foreach (var abilityType in abilityTypes)
            {
                var abilityInstance = (Ability)ScriptableObject.FindObjectOfType(abilityType);
                abilityInstance.id = allAbilities.Count;
                allAbilities.Add(abilityInstance);
                abilityTypesToIds[abilityType] = abilityInstance.id;
            }
        }

        protected virtual void OnValidate()
        {
            if (cooldown < 0)
            {
                cooldown = 0;
            }
            if (requiredLevel < 0)
            {
                requiredLevel = 0;
            }
            if (goldCost < 0)
            {
                goldCost = 0;
            }
            if (maxLevel < 1)
            {
                maxLevel = 1;
            }
        }
        #endregion
    }
}

public enum AbilityCastMode
{
    Normal,
    Channeled,
    Passive
}