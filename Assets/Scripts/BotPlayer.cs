﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

namespace Pugnacies
{
    public class NonHumanPlayer : InGamePlayer
    {
        public override string Name { get { return name; /* MonoBehaviour */ } }
    }
}