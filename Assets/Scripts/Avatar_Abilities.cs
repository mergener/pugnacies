﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;

namespace Pugnacies
{
    public partial class Avatar : MonoBehaviourPun, IPunObservable
    {
        public const int AbilitiesPerPlayer = 5;

        private class AvatarAbilityInfo
        {
            public float cooldown;
            public int level;
        }

        private Dictionary<int, AvatarAbilityInfo> abilityInfoTable = new Dictionary<int, AvatarAbilityInfo>();
        public List<Ability> Abilities
        {
            get
            {
                List<Ability> ret = new List<Ability>(abilityInfoTable.Count);

                foreach (var id in abilityInfoTable.Keys)
                {
                    ret.Add(Ability.GetWithID(id));
                }

                return ret;
            }
        }

        public bool HasAbility(Ability a)
        {
            return abilityInfoTable.ContainsKey(a.ID);
        }

        public float GetAbilityCurrentCooldown(Ability a)
        {
            return abilityInfoTable[a.ID].cooldown;
        }

        public int GetLevelOfAbility(Ability a)
        {
            return abilityInfoTable[a.ID].level;
        }

        public void SetLevelOfAbility(Ability a, int newLevel)
        {
            this.photonView.RPC("ApplyAbilityLevelChange", RpcTarget.All, a.ID, newLevel);
        }

        public void AddAbility(Ability a)
        {
            a.OnLearn(this);
            this.photonView.RPC("ApplyAbilityAddition", RpcTarget.All, a.ID);
        }

        public void AddAbility<A>() where A : Ability
        {
            AddAbility(Ability.Get<A>());
        }

        public void RemoveAbility<A>() where A : Ability
        {
            RemoveAbility(Ability.Get<A>());
        }

        public void RemoveAbility(Ability a)
        {
            a.OnLearn(this);
            this.photonView.RPC("ApplyAbilityRemoval", RpcTarget.All, a.ID);
        }

        public void PerformAbility<A>() where A : Ability
        {
            PerformAbility(Ability.Get<A>());
        }
        
        /// <summary>
        /// Performs the specified ability if it's not on cooldown, the Avatar is not busy/dead and it meets the requirements
        /// specified by the ability 'IsUsable' method.
        /// </summary>
        public void PerformAbility(Ability a)
        {
            if (HasAbility(a) && GetAbilityCurrentCooldown(a) <= 0 && a.IsUsable(this))
            {
                switch (a.CastMode)
                {
                    case AbilityCastMode.Normal:
                        a.OnUse(this);
                        photonView.RPC("ApplyAbilityCooldown", RpcTarget.All, a.ID);                        
                        break;

                    case AbilityCastMode.Channeled:
                        a.OnStartUse(this);
                        photonView.RPC("ApplyAbilityCooldown", RpcTarget.All, a.ID);                        
                        break;
                }
            }
        }

        [PunRPC]
        private void ApplyAbilityCooldown(int id)
        {
            abilityInfoTable[id].cooldown = Ability.GetWithID(id).Cooldown;
        }

        [PunRPC]
        private void ApplyAbilityRemoval(int id)
        {
            abilityInfoTable.Remove(id);
        }

        [PunRPC]
        private void ApplyAbilityAddition(int id)
        {
            abilityInfoTable[id] = new AvatarAbilityInfo { cooldown = 0f, level = 1 };
        }

        [PunRPC]
        private void ApplyAbilityLevelChange(int id, int newLevel)
        {
            abilityInfoTable[id].level = newLevel;
        }

        /// <summary>
        /// Returns true if purchase was succesful.
        /// </summary>
        public bool PurchaseAbility(Ability ab)
        {
            if (ab.GoldCost <= Owner.Gold && ab.RequiredLevel <= level)
            {
                Owner.Gold -= ab.GoldCost;
                AddAbility(ab);
                return true;
            }
            return false;
        }

        [UpdateOnly]
        private void CountAbilityCooldowns()
        {
            foreach (var k in abilityInfoTable.Keys)
            {
                abilityInfoTable[k].cooldown = abilityInfoTable[k].cooldown > 0 ? abilityInfoTable[k].cooldown - Time.deltaTime : 0;
            }
        }
    }
}