﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using System.Collections;
using Photon.Pun;
using UnityEngine;

namespace Pugnacies
{
    public class AutoMove : MonoBehaviourPun
    {
        public float speed = 2f;
        public float acceleration = 1f;
        public bool useTransformDirection = true;

        [SerializeField]
        private Vector3 direction = new Vector3(0, 0, 1);
        public Vector3 Direction
        {
            get { return direction; }
            set { direction = value.normalized; }
        }

        void OnValidate()
        {
            if (direction.x == 0 && direction.y == 0 && direction.z == 0)
            {
                Debug.Log(string.Format("Correcting direction vector on AutoMove of GameObject {0}, Vector cannot be zero.", gameObject.name));
            }

            direction.z = 1f;
        }

        void Awake()
        {
            direction.Normalize();
        }

        void Update()
        {
            if (useTransformDirection)
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
            else
            {
                transform.position += direction * speed * Time.deltaTime;
            }
        }
    }
}