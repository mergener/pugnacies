﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using System.Collections;
using Photon.Pun;
using UnityEngine;

namespace Pugnacies
{
    public class DestroyAfterInstantiation : MonoBehaviourPun
    {
        [SerializeField]
        private float destructionDelay;

        [SerializeField]
        private bool usePhotonDestroy = true;

        private IEnumerator DestructionCoroutine()
        {
            yield return new WaitForSeconds(destructionDelay);

            PhotonNetwork.Destroy(this.gameObject);
        }

        void OnValidate()
        {
            if (destructionDelay < 0)
            {
                destructionDelay = 0;
            }
        }

        void Start()
        {
            if (usePhotonDestroy)
            {
                StartCoroutine(DestructionCoroutine());
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
    }
}