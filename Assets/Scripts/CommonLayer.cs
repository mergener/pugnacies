﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using Photon.Pun;
using UnityEngine;

namespace Pugnacies
{
    [RequireComponent(typeof(PhotonView))]
    public class CommonLayer : MonoBehaviourPunCallbacks
    {
        private static CommonLayer singleton = null;
        public static CommonLayer Singleton { get { return singleton; } }
        
        void Awake()
        {
            if (singleton == null)
            {
                singleton = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else if (singleton != this)
            {
                Destroy(this.gameObject);
            }
        }
    }
}