﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using Photon.Realtime;
using System.Collections.Generic;

namespace Pugnacies
{
    using Team = List<Player>;
    public class Match
    {
        private static Match matchBeingPlayed = null;
        public static Match MatchBeingPlayed { get { return matchBeingPlayed; } }

        private bool playing = false;

        private int sceneID;
        public int SceneID { get { return sceneID; } }

        private List<Team> teams = new List<Team>();

        public Match()
        {

        }

        public void StartMatch()
        {

        }
    }
}