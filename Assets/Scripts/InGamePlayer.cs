﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using Photon.Pun;
using UnityEngine;

namespace Pugnacies
{
    [RequireComponent(typeof(PhotonView))]
    public abstract class InGamePlayer : MonoBehaviourPun
    {
        public abstract string Name { get; }

        protected Avatar avatar = null;
        public Avatar Avatar { get { return avatar; } }

        #region Data
        [System.Serializable]
        private class PlayerData
        {
            public int score = 0;
            public int kills = 0;
            public int totalDeaths = 0;
            public int deathsToLava = 0;
            public int deathsToPlayers = 0;
            public int otherDeaths = 0;
            public int assists = 0;
            public int damageDealt = 0;
            public int damageTaken = 0;
            public int team = 0;
            public int gold = 0;
        }

        [SerializeField]
        private PlayerData data = new PlayerData();

        public int Score
        {
            get { return data.score; }
            set { data.score = value; }
        }

        public int Kills
        {
            get { return data.kills; }
            set { data.kills = value; }
        }

        public int TotalDeaths
        {
            get { return data.totalDeaths; }
            set { data.totalDeaths = value; }
        }

        public int DeathsToLava
        {
            get { return data.deathsToLava; }
            set { data.deathsToLava = value; }
        }

        public int DeathsToPlayers
        {
            get { return data.deathsToPlayers; }
            set { data.deathsToPlayers = value; }
        }

        public int OtherDeaths
        {
            get { return data.otherDeaths; }
            set { data.otherDeaths = value; }
        }

        public int Assists
        {
            get { return data.assists; }
            set { data.assists = value; }
        }

        public int DamageDealt
        {
            get { return data.damageDealt; }
            set { data.damageDealt = value; }
        }

        public int DamageTaken
        {
            get { return data.damageTaken; }
            set { data.damageTaken = value; }
        }

        public int Team
        {
            get { return data.team; }
            set { data.team = value; }
        }

        public int Gold
        {
            get { return data.gold; }
            set { photonView.RPC("ApplyGoldChange", RpcTarget.All, value); }
        }

        public bool IsAllyOf(InGamePlayer other)
        {
            return data.team == other.data.team;
        }

        [PunRPC]
        private void ApplyGoldChange(int newGold)
        {
            if (newGold < 0)
            {
                data.gold = 0;
            }
            else
            {
                data.gold = newGold;
            }
        }
        #endregion Data

        protected virtual void Awake()
        {

        }

        protected virtual void Update()
        {
            
        }
    }
}