﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using Photon.Pun;
using System;

namespace Pugnacies
{ 
    /// <summary>
    /// Every wizard should contain this component.
    /// This class must be network safe, meaning that all its entire interface must work
    /// correctly within network.
    /// </summary>
    [RequireComponent(typeof(PhotonView))]
    [RequireComponent(typeof(PhotonTransformView))]
    [RequireComponent(typeof(Rigidbody))]
    [InSync]
    public partial class Avatar : MonoBehaviourPun, IPunObservable
    {
        private InGamePlayer owner;
        public InGamePlayer Owner
        {
            get { return owner; }
        }

        public const int MaximumLevel = 12;
        
        [SerializeField]
        private int level = 1;
        public int Level { get { return level; } }

        void Awake()
        {
            InitalizeHealthSerializer();
        }

        void OnValidate()
        {
            FacingAngle = facingAngle;
        }

        void Start()
        {
            StartCoroutine(RegenerateHealth());
        }

        void Update()
        {
            ProcessOrders();
            CountAbilityCooldowns();
            ApplyMovement();
            CountSpeedModifiersTimers();
        }

        void IPunObservable.OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            SerializeViewHealth(stream, info);
            SerializeViewMotion(stream, info);
        }
    }
}
