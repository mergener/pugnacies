﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Photon.Pun;

namespace Pugnacies
{
    public partial class Avatar : MonoBehaviourPun, IPunObservable
    {
        #region Moving
        private bool isWalking = false;
        public bool IsWalking { get { return CurrentOrder == AvatarOrder.Moving; } }

        private bool isRunning = false;
        public bool IsRunning { get { return isRunning; } }

        [SerializeField]
        private float walkSpeed = 2f;
        public float WalkSpeed
        {
            get { return walkSpeed; }
            set { photonView.RPC("ApplyWalkSpeedChange", RpcTarget.All, Mathf.Abs(value)); }
        }

        [PunRPC]
        private void ApplyWalkSpeedChange(float newSpeed)
        {
            walkSpeed = newSpeed;
        }

        [SerializeField]
        private float runSpeed = 4f;
        public float RunSpeed
        {
            get { return runSpeed; }
            set { photonView.RPC("ApplyRunSpeedChange", RpcTarget.All, Mathf.Abs(value)); }
        }

        [PunRPC]
        private void ApplyRunSpeedChange(float newSpeed)
        {
            runSpeed = newSpeed;
        }

        private void StartWalking()
        {
            SendOrder(AvatarOrder.Moving, true);
        }

        private Coroutine walkTowardsCoroutine;
        public void WalkTowards(Vector3 destination)
        {
            if (walkTowardsCoroutine != null)
                StopCoroutine(walkTowardsCoroutine);

            StartWalking();
            walkTowardsCoroutine = StartCoroutine(WalkTowardsCoroutine(destination));
        }

        private IEnumerator WalkTowardsCoroutine(Vector3 destination)
        {
            // treat the playable scenario pathing as bidimensional
            while (new Vector2(transform.position.x - destination.x, transform.position.z - destination.z).magnitude > 0.1f && IsWalking)
            {
                ForceFace(destination);

                yield return new WaitForEndOfFrame();
            }
            StopWalking();
        }

        public void StopWalking()
        {
            if (walkTowardsCoroutine != null)
                StopCoroutine(walkTowardsCoroutine);

            if (IsWalking)
            {
                StopCurrentOrder();
            }
        }

        #endregion Moving

        #region Facing
        public const float maximumTurnRate = 120f;
        public const float minimumTurnRate = 0.001f;

        [SerializeField]
        [Range(minimumTurnRate, maximumTurnRate)]
        private float turnRate = 2.5f;
        public float TurnRate
        {
            get { return turnRate; }
            set
            {
                float newTurnRate;
                if (value > maximumTurnRate)
                {
                    newTurnRate = maximumTurnRate;
                }
                else if (value < minimumTurnRate)
                {
                    newTurnRate = minimumTurnRate;
                }
                else
                {
                    newTurnRate = value;
                }
                photonView.RPC("ApplyTurnRateChange", RpcTarget.All, newTurnRate);
            }
        }
        [PunRPC]
        private void ApplyTurnRateChange(float newTurnRate)
        {
            turnRate = newTurnRate;
        }

        private Coroutine faceCoroutine = null;
        
        //
        // Since we do not keep states of current facing actions, we don't need to care about syncing
        // facing code with the network, just let the photonTransformView take care of it.
        //

        public void Face(Vector3 point)
        {
            var angle = Mathf.Atan2(point.x - transform.position.x, point.z - transform.position.z) * Mathf.Rad2Deg;
            var delay = Mathf.Abs(angle - facingAngle) / (turnRate * 360);

            if (faceCoroutine != null)
                StopCoroutine(faceCoroutine);

            faceCoroutine = StartCoroutine(FaceCoroutine(angle, delay));
        }

        public void Face(float angle)
        {
            var deltaAngle = facingAngle - angle;
            var delay = Mathf.Abs(deltaAngle) / (turnRate * 360);

            if (faceCoroutine != null)
                StopCoroutine(faceCoroutine);

            faceCoroutine = StartCoroutine(FaceCoroutine(angle, delay));
        }

        public void ForceFace(Vector3 point, float delay = 0f)
        {
            var angle = Mathf.Atan2(point.x - transform.position.x, point.z - transform.position.z) * Mathf.Rad2Deg;

            if (faceCoroutine != null)
                StopCoroutine(faceCoroutine);

            faceCoroutine = StartCoroutine(FaceCoroutine(angle, delay));
        }

        public void ForceFace(float angle, float delay = 0f)
        {
            if (faceCoroutine != null)
                StopCoroutine(faceCoroutine);

            faceCoroutine = StartCoroutine(FaceCoroutine(angle, delay));
        }

        private IEnumerator FaceCoroutine(float angle, float delay)
        {
            var count = 0f;
            if (angle - facingAngle > 180)
            {
                angle -= 360;
            }
            else if (angle - facingAngle < -180)
            {
                angle += 360;
            }
            float rotationPerSecond = Mathf.Abs((facingAngle - angle) / delay);

            while (count < delay)
            {
                FacingAngle += rotationPerSecond * Time.deltaTime;

                count += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            FacingAngle = angle;
        }
        
        [SerializeField]
        [Range(0, 360)]
        private float facingAngle = 0f;
        public float FacingAngle
        {
            get { return facingAngle; }
            set
            {
                facingAngle = value;

                if (syncFacingAngleWithRootRotation)
                {
                    transform.eulerAngles = new Vector3(transform.eulerAngles.x, facingAngle, transform.eulerAngles.z);
                }
            }
        }

        [SerializeField]
        private bool syncFacingAngleWithRootRotation = true;
        #endregion Facing

        #region Modifiers
        private class SpeedModifier
        {
            private float amount;
            private float duration;
            private bool isFactor;

            public float Amount { get { return amount; } }
            public float Duration
            {
                get { return duration; }
                set { duration = value; }
            }
            public bool IsFactor { get { return isFactor; } }

            private SpeedModifier() { }

            public SpeedModifier(float amount, float duration, bool isFactor)
            {
                this.amount = amount;
                this.duration = duration;
                this.isFactor = isFactor;
            }
        }

        private Dictionary<string, SpeedModifier> speedModifiers = new Dictionary<string, SpeedModifier>();

        /// <summary>
        /// Adds a speed modifier to this avatar. The string parameter is required if you are willing to manually
        /// remove this modifier later.
        /// </summary>
        public void AddSpeedModifier(float amount, float duration, bool isFactor)
        {
            // the caller hasn't specified a name for the modifier, so we'll be randoming an integer
            // and using it as the modifier's name.
            AddSpeedModifier(Random.Range(0, 99999999).ToString(), amount, duration, isFactor);
        }

        /// <summary>
        /// Adds a speed modifier to this avatar. The string parameter is required if you are willing to manually
        /// remove this modifier later.
        /// </summary>
        public void AddSpeedModifier(string name, float amount, float duration, bool isFactor)
        {
            photonView.RPC("ApplySpeedModifierAddition", RpcTarget.All, name, amount, duration, isFactor);
        }

        [PunRPC]
        private void ApplySpeedModifierAddition(string name, float amount, float duration, bool isFactor)
        {
            speedModifiers[name] = new SpeedModifier(amount, duration, isFactor);
        }

        public void RemoveSpeedModifier(string name)
        {
            photonView.RPC("ApplySpeedModifierRemoval", RpcTarget.All, name);
        }

        [PunRPC]
        private void ApplySpeedModifierRemoval(string name)
        {
            try
            {
                speedModifiers.Remove(name);
            }
            catch (KeyNotFoundException )
            {

            }
        }

        /// <summary>
        /// Returns the speed after the effects of all modifiers.
        /// </summary>
        public float CalculateModifiedSpeed(float speed)
        {
            var factor = 1f;
            var sum = 0f;

            foreach (var sm in speedModifiers.Values)
            {
                if (sm.IsFactor)
                {
                    factor += sm.Amount;
                }
                else
                {
                    sum += sm.Amount;
                }
            }

            return speed * factor + sum;
        }
        #endregion Modifiers

        [UpdateOnly]
        private void CountSpeedModifiersTimers()
        {
            foreach (var sm in speedModifiers)
            {
                sm.Value.Duration -= Time.deltaTime;
                if (sm.Value.Duration <= 0)
                {
                    speedModifiers.Remove(sm.Key);
                }
            }
        } 

        [UpdateOnly]
        private void ApplyMovement()
        {
            if (IsWalking && photonView.IsMine)
            {
                float speed = CalculateModifiedSpeed(isRunning ? runSpeed : walkSpeed) * Time.deltaTime;

                Vector3 direction = new Vector3 (
                    Mathf.Sin(Mathf.Deg2Rad * FacingAngle),
                    0,
                    Mathf.Cos(Mathf.Deg2Rad * FacingAngle));

                transform.position += speed * direction;
            }
        }

        #region Events and Delegates
        public delegate void OnStartWalkingDel(Avatar a);
        public event OnStartWalkingDel OnStartWalking;
        public delegate void OnStopWalkingDel(Avatar a);
        public event OnStopWalkingDel OnStopWalking;

        public delegate void OnStartRunningDel(Avatar a);
        public event OnStartRunningDel OnStartRunning;
        public delegate void OnStopRunningDel(Avatar a);
        public event OnStopRunningDel OnStopRunning;
        #endregion Events and Delegates

        private void SerializeViewMotion(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(facingAngle);
                stream.SendNext(isWalking);
                stream.SendNext(isRunning);
            }
            else
            {
                FacingAngle = (float)stream.ReceiveNext();
                isWalking = (bool)stream.ReceiveNext();
                isRunning = (bool)stream.ReceiveNext();
            }
        }
    }
}