﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;

namespace Pugnacies
{
    using PhotonPlayer = Photon.Realtime.Player;

    public class InGameLayer : MonoBehaviourPun
    {
        private static float elapsedTime = 0f;
        public static float ElapsedTime { get { return elapsedTime; } }

        private static InGameLayer singleton;
        public static InGameLayer Singleton { get { return singleton; } }
        
        [SerializeField]
        private Vector3 playerSpawnLocation;
        public static Vector3 PlayerSpawnLocation { get { return singleton.playerSpawnLocation; } }

        private List<PhotonPlayer> players;

        #region Initialization
        private void GeneratePlayers()
        {
            var room = PhotonNetwork.CurrentRoom;

            players = new List<PhotonPlayer>(room.PlayerCount);

            foreach (var p in room.Players.Values)
            {
                players.Add(p);
            }

            PhotonNetwork.Instantiate("in_game_player", Vector3.zero, Quaternion.identity);
        }

        private void GenerateMatch()
        {
        }
        #endregion Initialization

        void Awake()
        {
            if (singleton == null)
            {
                singleton = this;
            }
            else if (singleton != this)
            {
                throw new SingletonDuplicateException();
            }

            GeneratePlayers();
            GenerateMatch();
        }

        void Update()
        {
            elapsedTime += Time.deltaTime;
        }
    }
}