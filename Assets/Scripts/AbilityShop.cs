﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;

namespace Pugnacies
{
    public class AbilityShop : MonoBehaviour
    {
        public static AbilityShop Singleton { get; private set; }
        
        void Awake()
        {
            if (Singleton == null)
            {
                Singleton = this;
            }
            else
            {
                throw new SingletonDuplicateException();
            }
        }
    }
}