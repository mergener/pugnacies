﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;

namespace Pugnacies
{
    public static class NavigationCalculator
    {
        /// <summary>
        /// (To be made) Calculates a path to a certain destination point.
        /// </summary>
        /// <param name="rb"> The rigidbody attached to the object that requires calculation. </param>
        /// <param name="destination"> The destination point. </param>
        /// <returns>The ordered array of points.</returns>
        public static Vector2[] GetRoute(Rigidbody2D rb, Vector2 destination)
        {
            return new Vector2[] { destination };
        }
    }
}