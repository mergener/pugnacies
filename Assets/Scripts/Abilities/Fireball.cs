// Fireball
using UnityEngine;
using Photon.Pun;

namespace Pugnacies
{
    public class Fireball : Ability 
	{
        [SerializeField]
        [Tooltip("The fireball prefab to be instantiated. Must be at Resources/ path or under.")]
        private string fireballPrefabName;

		[SerializeField]
		private float[] fireballDamage;
		public static float GetDamage(int level)
		{
			return Ability.Get<Fireball>().fireballDamage[level];
		}

        [SerializeField]
        private float[] fireballDuration;
		public static float GetDuration(int level)
		{
			return Ability.Get<Fireball>().fireballDuration[level];
		}

        [SerializeField]
        private float[] fireballForce;
		public static float GetForce(int level)
		{
			return Ability.Get<Fireball>().fireballForce[level];
		}

		protected override void OnValidate()
		{
			base.OnValidate();

			if (fireballDuration == null)
			{
				fireballDuration = new float[MaxLevel];
			}
			else if (fireballDuration.Length < MaxLevel)
			{
				fireballDuration = new float[MaxLevel];
			}

			if (fireballForce == null)
			{
				fireballForce = new float[MaxLevel];
			}
			else if (fireballForce.Length < MaxLevel)
			{
				fireballForce = new float[MaxLevel];
			}
		}
        
		// called when Avatar a uses this ability at object target
		public override void OnUse(Avatar a, object target = null)
		{
		}
    
		// called when Avatar a acquires this ability
		public override void OnLearn(Avatar a)
		{
    
		}
    
		// called when Avatar a unlearns this ability
		public override void OnUnlearn(Avatar a)
		{
    
		}

		// called when Avatar a levels this skill up from oldLevel to newLevel
		public override void OnLevelChange(Avatar a, int oldLevel, int newLevel)
		{
	
		}
    
		// return true if Avatar a should be able to cast this ability
		public override bool IsUsable(Avatar a, object target = null)
		{
			return true;
		}

		public static readonly AvatarOrder FireballOrder = new AvatarOrder(
			true,		// is interruptable?
			0f			// duration
		);
    }
}