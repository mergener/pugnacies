// New Ability
using UnityEngine;

namespace Pugnacies
{
	public class NewAbility : Ability 
	{
		// called when Avatar a begins channeling this ability
		public override void OnStartUse(Avatar a, object target = null) 
		{
	
		}

		// called when Avatar a cancels the use of this ability
		public override void OnCancelUse(Avatar a, object target = null)
		{
        
		}
    
		// called when Avatar a finishes using this ability upon object target
		public override void OnFinishUse(Avatar a, object target = null)
		{
    
		}
    
		// called when Avatar a acquires this ability
		public override void OnLearn(Avatar a)
		{
    
		}
    
		// called when Avatar a unlearns this ability
		public override void OnUnlearn(Avatar a)
		{
    
		}

		// called when Avatar a levels this skill up from oldLevel to newLevel
		public override void OnLevelChange(Avatar a, int oldLevel, int newLevel)
		{
	
		}
    
		// return true if Avatar a should be able to cast this ability
		public override bool IsUsable(Avatar a, object target = null)
		{
			return true;
		}

		public static readonly AvatarOrder NewAbilityOrder = new AvatarOrder(
			true,		// is interruptable?
			0f			// duration
		);
	}
}