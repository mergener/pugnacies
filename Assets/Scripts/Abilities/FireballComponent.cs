﻿using UnityEngine;
using System.Collections;

namespace Pugnacies
{
	[RequireComponent(typeof(Rigidbody))]
	public class FireballComponent : MonoBehaviour
	{
		private float damage;
		public float Damage 
		{
			get { return damage; }
			set { damage = value; }
		}

        public Avatar Sender { get; set; }
		private const float durationCounter = 5f;

		private IEnumerator DeathCoroutine()
		{
			yield return new WaitForSeconds(durationCounter);
			Destroy(this.gameObject);
		}

		void OnCollisionEnter(Collision coll)
		{
			var avatar = coll.gameObject.GetComponent<Avatar>();
			if (avatar)
			{
				if (!avatar.Owner.IsAllyOf(this.Sender.Owner))
				{
					avatar.TakeDamage(Fireball.GetDamage(avatar.GetLevelOfAbility( Fireball.Get<Fireball>() ) ), this.Sender, DamageType.Fire);
				}
			}
		}

		void Start()
		{
			StartCoroutine(DeathCoroutine());
		}
	}
}
