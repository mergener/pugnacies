﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/


namespace Pugnacies
{
    using PhotonPlayer = Photon.Realtime.Player;

    public abstract class MessageChannel
    {
        public delegate void OnReceiveMessageDel(ChannelMessage message);
        public event OnReceiveMessageDel OnReceiveMessage;
        
        protected void RaiseMessageOutputEvent(ChannelMessage message)
        {
            OnReceiveMessage(message);
        }

        public abstract void SendMessage(ChannelMessage message);
        public abstract ChannelMessage GetLastMessage();
        public abstract ChannelMessage[] GetAllMessages();
        public abstract void Flush();
    }

    public struct ChannelMessage
    {
        private string message;
        private PhotonPlayer sender;

        public string Message { get { return message; } }
        public PhotonPlayer Sender { get { return sender; } }

        public ChannelMessage(string message, PhotonPlayer sender)
        {
            this.message = message;
            this.sender = sender;
        }
    }
}