﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using Photon.Pun;
using System.Collections.Generic;

namespace Pugnacies
{
    public partial class Avatar : MonoBehaviourPun, IPunObservable
    {
        private Queue<AvatarOrder> avatarOrderQueue = new Queue<AvatarOrder>();
        private AvatarOrder currentOrder = AvatarOrder.Idle;
        public AvatarOrder CurrentOrder { get { return currentOrder; } }
        private float orderTimer = 0f;

        /// <summary>
        /// Sends this order to the unit queue. Returns the queue position the order was inserted.
        /// If skipQueue is true and the current order is not interruptable, the sent order will be ignored.
        /// </summary>
        public void SendOrder(AvatarOrder order, bool skipQueue = false)
        {
            if (order == AvatarOrder.Idle)
                return;

            if (!skipQueue)
            {
                if (currentOrder == AvatarOrder.Idle)
                {
                    // if queue is empty, go directly
                    photonView.RPC("SetOrder", RpcTarget.All, order.ID);
                }
                else
                {
                    avatarOrderQueue.Enqueue(order);
                }
            }
            else
            {
                // skip the queue...
                if (CurrentOrder.Interruptable)
                {
                    photonView.RPC("SetOrder", RpcTarget.All, order.ID);
                }
            }
        }

        public void StopCurrentOrder(bool force = false)
        {
            if (!force && !CurrentOrder.Interruptable)
                return;

            if (avatarOrderQueue.Count > 0)
            {
                photonView.RPC("SetOrder", RpcTarget.All, avatarOrderQueue.Dequeue().ID);
            }
            else
            {
                photonView.RPC("SetOrder", RpcTarget.All, AvatarOrder.Idle.ID);
            }
        }

        [PunRPC]
        private void SetOrder(int orderID)
        {
            var order = AvatarOrder.GetWithID(orderID);

            currentOrder = order;
            orderTimer = order.Duration;
        }

        [UpdateOnly]
        private void ProcessOrders()
        {
            if (currentOrder.Duration < 0f)
                return; // order has indefinite duration

            if (orderTimer > 0f)
            {
                orderTimer -= Time.deltaTime;

                if (orderTimer <= 0f)
                {
                    if (photonView.IsMine)
                        StopCurrentOrder(force: true);
                }
            }
        }
    }
}