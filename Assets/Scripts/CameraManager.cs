﻿/*
 *                                                         
 *  Pugnacies Arena                                        
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using Photon.Pun;
using System.Collections;

namespace Pugnacies
{
    [RequireComponent(typeof(PhotonView))]
    [InSync]
    public class CameraManager : MonoBehaviourPun
    {
        private static CameraManager currentCamera;
        public static CameraManager CurrentCamera { get { return currentCamera; } }

        public static void ShakeCamera(float magnitude, float duration, HumanPlayer player)
        {
            currentCamera.photonView.RPC("ApplyCameraShake", player.PhotonPlayer, magnitude, duration);
        }

        public static void ShakeCamera(float magnitude, float duration, RpcTarget rpcTarget)
        {
            currentCamera.photonView.RPC("ApplyCameraShake", rpcTarget, magnitude, duration);
        }

        public static void ShakeCamera(float magnitude, float duration)
        {
            currentCamera.ApplyCameraShake(magnitude, duration);
        }

        [PunRPC]
        private void ApplyCameraShake(float magnitude, float duration)
        {
            StartCoroutine(ShakeCameraCoroutine(magnitude, duration));
        }

        private IEnumerator ShakeCameraCoroutine(float magnitude, float duration)
        {
            yield return null; // TO BE DONE
        }

        void Awake()
        {
            currentCamera = this;
        }

        void LateUpdate()
        {
            const float lerpConstant = 0.05f;
            const float offsetDistance = 10f;

            // follow player:
            transform.position = new Vector3 (
                Mathf.Lerp(transform.position.x, HumanPlayer.LocalPlayer.Avatar.transform.position.x, lerpConstant),
                transform.position.y,
                Mathf.Lerp(transform.position.z, HumanPlayer.LocalPlayer.Avatar.transform.position.z - offsetDistance, lerpConstant));
            
        }
    }
}