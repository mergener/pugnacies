﻿/*
 *  
 *  Copyright (C) 2019 Thomas Mergener - All rights reserved.   
 *                                                         
*/

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Reflection;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Pugnacies;

public class AbilityCreator : EditorWindow
{
    // template paths:
    const string constTemplateNormal = "Assets/Editor/AbilityCreator Templates/NormalCastAbility.cs.template";
    const string constTemplatePassive = "Assets/Editor/AbilityCreator Templates/PassiveAbility.cs.template";
    const string constTemplateChanneled = "Assets/Editor/AbilityCreator Templates/ChanneledAbility.cs.template";

    const string tempFilePath = "Assets/Editor/TEMP.abilitycreator";

    private static readonly Regex regex = new Regex("^[a-zA-Z ]*$");
    private static AbilityCreator window;
    private static string abilityName = "New Ability";
    private static AbilityCastMode abilCastMode = AbilityCastMode.Normal;
    
    [DidReloadScripts]
    private static void AssembleAbilities()
    {
        if (!File.Exists(tempFilePath)) return; 

        var assembly = Assembly.GetAssembly(typeof(Ability));
        var abils = from abil in assembly.GetTypes()
                    where abil.IsSubclassOf(typeof(Ability)) && !abil.IsAbstract
                    select abil;

        var file = new StreamReader(tempFilePath);
        var abilClass = file.ReadLine();
        var abilName = file.ReadLine();

        int i;
        for (i = 1; File.Exists("Assets/Abilities/" + abilName + (i > 1 ? i.ToString() : string.Empty) + ".asset"); i++);
        if (i != 1)
        {
            abilName += i;
        }

        foreach (var abil in abils)
        {
            if (abilClass != abil.Name) continue;
            var path = "Assets/Abilities/" + abilName + ".asset";
            var so = ScriptableObject.CreateInstance(abil);
            var soab = so as Ability;

            soab.name = abilName;
            soab.CastMode = (AbilityCastMode)int.Parse(file.ReadLine());
            file.Close();
            File.Delete(tempFilePath);
            
            AssetDatabase.CreateAsset(so, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            break;
        }
    }

    [MenuItem("Custom Tools/New Ability")]
    public static void StartWindow()
    {
        window = GetWindow<AbilityCreator>();
        window.minSize = new Vector2(315, 150);
        window.maxSize = new Vector2(315, 150);
        window.Show();
    }

    void OnGUI()
    {
        GUI.enabled = true;
        GUILayout.Label("Ability Creator", EditorStyles.boldLabel);
        GUILayout.Label (
            "This will create a script and a ScriptableObject for \nyour spell." +
            "\n\nThe script is saved at 'Assets/Scripts/Abilities', \nwhilst the ScriptableObject is saved at 'Assets/Abilities'",
            new GUIStyle { fixedHeight = 65 }
        );
        
        abilityName = EditorGUILayout.TextField (
            "Ability name:", 
            abilityName
        );

        abilCastMode = (AbilityCastMode)EditorGUILayout.EnumPopup("Cast mode: ", abilCastMode);
        
        GUI.enabled = regex.IsMatch(abilityName);
        if (GUILayout.Button("Create!"))
        {
            var abilNameNoSpace = abilityName.Replace(" ", string.Empty);
            var filepath = "Assets/Scripts/Abilities/";

            int i;
            for (i = 1; File.Exists(filepath + abilNameNoSpace + (i > 1 ? i.ToString() : string.Empty) + ".cs"); i++);
            if (i > 1)
            {
                abilNameNoSpace += i;
            }
            filepath += abilNameNoSpace + ".cs";

            switch (abilCastMode)
            {
                case AbilityCastMode.Normal:
                    File.Copy(constTemplateNormal, filepath);
                break;
                
                case AbilityCastMode.Channeled:
                    File.Copy(constTemplateChanneled, filepath);
                    break;
                
                case AbilityCastMode.Passive:
                    File.Copy(constTemplatePassive, filepath);
                    break;
            }

            var swap = File.ReadAllText(filepath);       
            swap = swap.Replace("%SKILLNAME%", abilityName);
            swap = swap.Replace("%SKILLCLASS%", abilNameNoSpace);
            File.WriteAllText(filepath, swap);

            var fs = File.Create(tempFilePath, 100);
            var content = Encoding.ASCII.GetBytes(abilNameNoSpace + "\n" + abilityName + "\n" + (int)abilCastMode);
            fs.Write(content, 0, content.Length);
            fs.Close();

            AssetDatabase.Refresh();

            if (window != null)
                window.Close();
        }
    }
}